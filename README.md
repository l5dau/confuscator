Confuscates the data in given CTE docunments, means it removes all human readable information but keeps structure of the file.


## How to use 

* Move your file to the input folder
* Change some words in the 'lorem_ipsum.txt' file 
* Run `./gradlew run` or `gradlew.bat run`
* give the file in output to the razorcat support team. 