
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class ConfuscateCTEData {

	private Path path;
	private Map<String, String> alreadySeen = new HashMap<>();
	private static Iterator<String> LOREM_IPSUM = Collections.emptyIterator();
	private static final Pattern STRINGS_PATTERN = Pattern.compile("\".*\"");
	private static final Logger LOGGER = Logger.getGlobal();

	public ConfuscateCTEData(Path path) {
		this.path = path;
	}

	private static void initLoremIpsum() {
		try {
			LOREM_IPSUM = Files.lines(Paths.get("lorem_ipsum.txt")).flatMap(str -> Arrays.stream(str.split("\\s")))
					.iterator();
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	public static void main(String[] args) throws IOException {

		Path cteFilesRoot = Paths.get("input");

		Path outputDir = Paths.get("output");
		if (!Files.exists(outputDir)) {
			Files.createDirectory(outputDir);
		}

		// @formatter:off
		Files.list(cteFilesRoot).filter(path -> path.toString().endsWith(".cte")).forEach(path -> {
			try {
				new ConfuscateCTEData(path).replaceCustomerData();
			} catch (Exception e) {
				LOGGER.log(Level.SEVERE, e, () -> "while processing " + path.toString());
			}
		});
		// @formatter:on
	}

	private void replaceCustomerData() {

		try {
			Stream<String> linesOfFile = Files.lines(path).map(line -> replaceStrings(line));
			// @formatter:off
			Files.write(Paths.get("output").resolve(path.getFileName()),
					(Iterable<String>) () -> linesOfFile.iterator());
			// @formatter:on
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	private String replaceStrings(String line) {
		Matcher m = STRINGS_PATTERN.matcher(line);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			m.appendReplacement(sb, replacementFor(m.group()));
		}
		m.appendTail(sb);
		return sb.toString();
	}

	private String replacementFor(String group) {
		if (alreadySeen.containsKey(group)) {
			return alreadySeen.get(group);
		}
		int length = group.length();
		StringBuilder replacment = new StringBuilder(length);
		replacment.append('"');
		while (replacment.length() < length) {
			replacment.append(nextReplacment()).append(" ");
		}
		replacment.setLength(length - 1);
		replacment.append('"');

		String ipsumText = replacment.toString();
		alreadySeen.put(group, ipsumText);
		return ipsumText;
	}

	private String nextReplacment() {
		if (!LOREM_IPSUM.hasNext()) {
			initLoremIpsum();
		}
		String next = LOREM_IPSUM.next();

		return next;
	}

}
